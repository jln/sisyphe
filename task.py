from datetime import date, datetime, timedelta
import re
import uuid

PRIORITY = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
REG_DATE = r"\d{4}-\d{2}-\d{2}"
REG_PRIORITY = r"\([A-Z]\)"
REG_UUID = r"[A-Fa-f0-9]{8}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{12}"

class Task():
    """ Note : les fx set_* attendent des données "absolues" : une date pour la
    création ou mise à jour d'une échéance, une priorité pour la création ou la
    mise à jour d'une échéance. Les fx update_* attendent des données
    "relatives" : un nb de jours pour modifier la date d'échéance, une
    "direction" pour abaisser ou augmenter une priorité
    """

    def __init__(self, content):
        self.content = content
        self.priority = ""
        self.creation_date = ""
        self.completion_date = ""
        self.body = ""
        self.context = ""
        self.project = ""
        self.duedate= ""
        self.delay = ""
        self.uuid = ""
        self.done = False
        self._parse_task()
 
    def _parse_task(self):
        """ Analyse globale de la tâche. La fonction :
        - analyse s'il s'agit d'une tâche done ou pas
        - assigne les attributs à l'instance en cours :
            - self.priority (pour les tâches non done)
            - self.creation_date
            - self.body
            - self.duedate
            - self.uuid
        - appelle la création de l'uuid si inexistant
        - appelle le parsing de body pour l'assignation
          des autres attributs (context, projet, duedate)
        - appelle le calcul du délai

        Cette fonction est appelée lors de la création d'une instance,
        ou bien lors de la modification "manuelle" d'une tâche (via la fx
        update_task, et quand une tâche bascule done)
        """
        if self.content.startswith("x "):
            pattern = re.compile(rf"^x ({REG_DATE} )?({REG_DATE} )?(.*)$")
            task_done = re.match(pattern, self.content)
            self.done = True
            self.completion_date = task_done.group(1)[:-1] if task_done.group(1) else ""
            self.creation_date = task_done.group(2)[:-1] if task_done.group(2) else ""
            self.body = task_done.group(3)

            self._parse_body()
        else:
            self.done = False
            
            # capture de l'uuid
            # celui-ci peut déjà exister si la tâche est updatée
            if not self.uuid:
                pattern = re.compile(rf" uuid:{REG_UUID}")
                uuid = re.search(pattern, self.content)
                if uuid:
                    self.uuid = uuid.group(0)[6:]
                    self.content = self.content.replace(uuid.group(0), "")
                else:
                    self._set_uuid()
                
            # capture de la due date
            pattern = re.compile(rf" due:{REG_DATE}")
            duedate = re.search(pattern, self.content)
            if duedate:
                self.duedate = duedate.group(0)[5:]
                self.content = self.content.replace(duedate.group(0), "")
                self._compute_delay()

            # capture directe du reste de la tâche
            pattern = re.compile(rf"({REG_PRIORITY} )?({REG_DATE} )?(.*)")
            task = re.match(pattern, self.content)
            self.priority = task.group(1)[:-1] if task.group(1) else ""
            self.creation_date = task.group(2)[:-1] if task.group(2) else ""
            self.body = task.group(3)

            self._parse_body()

    def _parse_body(self):
        """ Analyse le body, c'est à dire la description
        de la tâche (sans priorité, sans creation_date, sans uuid)
        afin d'assigner :
        - self.context
        - self.project
        """
        project = re.search(r"\+\w+", self.body)
        if project:
            self.project = project.group(0)

        context = re.search(r"@\w+", self.body)
        if context:
            self.context = context.group(0)

    def _compute_delay(self):
        duedate = self.duedate.split("-")
        delay = date(int(duedate[0]),
                     int(duedate[1]),
                     int(duedate[2])) - date.today()
        self.delay = delay.days

    def _set_uuid(self):
        """ Crée un uuid et l'assigne à self.uuid
        """
        self.uuid = str(uuid.uuid4())

    def task(self):
        """ Génère une tâche complète en fonction des attributs et du statut
        (done ou pas)
        """
        if self.done:
            cre_date = self.creation_date + " " if self.creation_date else ""
            duedate = " due:" + self.duedate if self.duedate else ""
            uuid = " uuid:" + self.uuid if self.uuid else ""
            task = f"x {self.completion_date} {cre_date}{self.body}{duedate}{uuid}"
        else:
            pri = self.priority + " " if self.priority else ""
            cre_date = self.creation_date + " " if self.creation_date else ""
            body = self.body.rstrip()
            duedate = " due:" + self.duedate if self.duedate else ""

            task = f"{self.priority}{self.creation_date}{self.body}{self.duedate} uuid:{self.uuid}"
            task = f"{pri}{cre_date}{body}{duedate} uuid:{self.uuid}"

        return task

    def set_creation_date(self):
        self.creation_date = date.today().strftime("%Y-%m-%d")

    def set_priority(self, priority):
        """ Positionne ou modifie la priorité d'une tâche. La fonction se
        contente d'assigner ou ré-assigner self.priority
        TODO : contrôle de la conformité de la valeur
        """
        self.priority = priority

    def set_content(self, content):
        """ Assigne un nouveau contenu à la tâche (typiquement, parce que
        celle-ci a été modifiée manuellement) et appelle son parsing
        On vide les attributs existants sauf l'uuid,
        afin qu'ils ne polluent pas la "nouvelle" tâche
        """
        self.priority = ""
        self.creation_date = ""
        self.body = ""
        self.duedate = ""
        self.content = content
        self._parse_task()

    def set_context_or_project(self, newvalue):
        """ Crée ou modifie un contexte ou un projet. L'analyse du premier caractère
        de newvalue détermine s'il s'agit de l'un ou l'autre. Si l'entité à changer est
        déjà existante, on la modifie pour la laisser en place dans
        le body. Sinon, on la positionne à la fin du body.

        TODO : gérer une erreur de valeur
        """
        mode = newvalue[:1]
        if mode == "@":
            if self.context:
                self.body = self.body.replace(self.context, newvalue)
            else:
                self.body = self.body + " " + newvalue
            self.context = newvalue
        elif mode == "+":
            if self.project:
                self.body = self.body.replace(self.project, newvalue)
            else:
                self.body = self.body + " " + newvalue
            self.project = newvalue
        else:
            print("ERROR")        

    def set_done(self):
        if not self.done:
            self.completion_date = date.today().strftime("%Y-%m-%d")
            self.priority = ""
            self.done = True

    def set_duedate(self, newdate):
        """ Crée ou modifie une date d'échéance.
        TODO : contrôler la conformité de la valeur
        """
        self.duedate = newdate

    def update_duedate(self, days):
        """ Met à jour une date d'échéance en lui ajoutant un nombre de jours.
        Si aucune date d'échéance ne pré-existe, le calcul est basé sur la date
        du jour
        """
        if not self.duedate:
            self.duedate = date.today().strftime("%Y-%m-%d")

        duedate = self.duedate.split("-")
        duedate = date(int(duedate[0]), int(duedate[1]), int(duedate[2]))
        duedate = duedate + timedelta(days=int(days))

        self.duedate = duedate.strftime("%Y-%m-%d")
        self._compute_delay()

    def update_priority(self, direction):
        """ Met à jour une priorité en l'augmentant ou la diminuant d'un cran.
        Si aucune priorité ne pré-existe, on fixe la priorité à (A) 
        """
        if not self.priority:
            self.priority = "(A)"
        else:
            priority_index = PRIORITY.index(self.priority[1])
            if (direction == 1 and self.priority != "(A)") or (direction == -1 and
                                                              self.priority !=
                                                              "(Z)"):
                new_priority = "(" + PRIORITY[priority_index - direction] + ")"
                self.priority = new_priority


