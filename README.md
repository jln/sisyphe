# Sisyphe - Gestion de tâches au format todotxt

(ver. 2020-12-04)

Sisyphe est un logiciel de gestion de tâches. Celles-ci sont décrites à l'aide de
la [syntaxe todo.txt](https://github.com/todotxt/todo.txt) et enregistrées dans
un fichier `todo.txt` qui présente l'avantage d'être pérenne (il s'agit de texte
"brut"), léger, facilement éditable et synchronisable, à l'aide par exemple d'un
logiciel comme Nextcloud.

Outre l'ajout et l'archivage de tâches, Sisyphe propose des fonctionnalités de
filtre et de tri. Il ajoute également à la syntaxe todo.txt la possibilité de
préciser une date d'échéance (sous la forme `due:YYYY-MM-DD`).

Sisyphe est conçu pour que la manipulation des tâches soit aisée et surtout
*rapide* : des raccourcis clavier permettent ainsi, en une touche, d'archiver
une tâche, d'en modifier la priorité, ou encore d'en modifier la date
d'échéance.

(Ce texte utilise le féminin générique)

## Sommaire

- [Installation et premier lancement](#installation-et-premier-lancement)
- [Description détaillée et usage](#description-détaillée-et-usage)
    - [Ajout de tâches](#ajout-de-tâches)
    - [Modification de tâches](#modification-de-tâches)
        - [Modification de la priorité](#modification-de-la-priorité)
        - [Modification de la date d'échéance](#modification-de-la-date-d%C3%A9ch%C3%A9ance)
    - [Archivage](#archivage)
    - [Tri](#tri)
    - [Filtre](#filtre)
    - [Coloration des délais](#coloration-des-délais)
- [Nouveautés](#nouveautés)
- [À venir](#à-venir)
- [Comment bien utiliser un tel logiciel de gestions de tâches ?](#comment-bien-utiliser-un-tel-logiciel-de-gestions-de-t%C3%A2ches-)

## Installation et premier lancement

Pour le moment, l'installation nécessite quelques manipulations. Tout d'abord,
il convient de clôner ce dépôt dans le répertoire de votre choix, puis d'entrer
dans le répertoire `sisyphe`:

```sh
git clone https://framagit.org/jln/sisyphe.git
cd sisyphe
```

Ensuite, on crée un environnement virtuel. La commande `python3.9` ci-dessous
est à remplacer par la version de Python installée sur votre machine :

```sh
python3.9 -m venv env
```

Puis on active cet environnement virtuel :

```sh
source env/bin/activate
```

On installe les modules nécessaires au bon fonctionnement de Sisyphe :

```sh
pip install PySide2 datetime uuid
```

Il est alors enfin possible de lancer Sisyphe :

```sh
python3.9 sisyphe.py
```

Au premier lancement, une fenêtre s'ouvrira pour demander où sont situés les
fichiers `todo.txt` et `done.txt` qui "stockent", respectivement, les tâches
actives, et les tâches archivées. Si ces fichiers n'existent pas encore, ils
seront automatiquement créés.

Pour les lancements suivants, il conviendra de bien "activer" l'environnement
virtuel avant de lancer Sisyphe :

```sh
source env/bin/activate
python3.9 sisyphe.py
```

## Description détaillée et usage

### Ajout de tâches

L'ajout d'une tâche s'effectue simplement par la saisie de celle-ci dans le
champ idoine. Elle est validée par un clic sur bouton `Ajouter`, ou un appui sur
la touche `Entrée`.

![Ajout d'une tâche](img/sisyphe-ajout-tache.gif)

Respectant la syntaxe todo.txt, la seule valeur absolument obligatoire est le
"corps" de la tâche. Les autres données (priorité, date de création, date
d'échéance) sont facultatives. Notez que Sisyphe insérera automatiquement la
date de création de la tâche. L'utilisatrice peut la saisir elle-même, par
exemple pour la personnaliser, si elle ne souhaite pas que la date de création
soit la date du jour.

### Modification de tâches

Les tâches sont éditables *via* un double-clic. Chaque élément est éditable de
la sorte¹. Attention : Sisyphe ne contrôle pas encore la validité des informatons
saisies (par exemple, la saisie d'une priorité sans parenthèses ne génèrera pas
d'erreur… mais n'aboutira sans doute pas au résultat attendu !)

(¹ à l'exception du délai ; pour le modifier, il suffit de modifier la date
d'échance.)

![Modification d'une tâche](img/sisyphe-modif-tache.gif)

Il est également possible de modifier certains éléments à l'aide de raccourcis
clavier.

#### Modification de la priorité

Une fois sélectionnée, une tâche peut voir sa priorité modifiée à l'aide des
touche `+` et `-` permettant, respectivement, de l'augmenter ou de la diminuer.
Si la tâche ne possède aucune priorité, la priorité `(A)` est appliquée et peut
être aisément modifiée.

![Modification d'un priorité](img/sisyphe-modif-pri.gif)

#### Modification de la date d'échéance

Les touches `<` et `>` permettent respectivement d'avancer ou reculer la date
d'échance de la tâche sélectionnée. Si cette dernière n'a aucune date d'échéance
renseignée, alors la date du jour est utilisée comme date d'échéance.

![Modification d'une date d'échéance](img/sisyphe-modif-duedate.gif)

### Archivage

L'archivage consiste à déclarer une tâche comme effectuée. Elle disparaît alors
de l'interface, est supprimée du fichier `todo.txt` et ajoutée au fichier
`done.txt`.

Il existe deux moyens d'archiver une tâche :

- éditer la priorité ou le corps de la tâche et ajouter `x ` devant l'élément
  édité ;
- sélectionner la touche et pressez la touche `x` du clavier.

![Archivage d'une tâche](img/sisyphe-archivage.gif)

Afin de respecter la syntaxe todo.txt, la priorité d'une tâche est supprimée
avant son archivage. De plus, la date de l'achivage est ajoutée à la tâche.

### Tri

Le tri des tâches s'effectue par un simple clic sur la colonne correspondant au
critère de tri voulu. Un second clic permet de trier en sens invese. Par défaut,
Sisyphe tri les tâches par priorité (les tâches les plus prioritaires étant
positionnées en haut de la liste).

**Problèmes connus** :

- le tri sur la colonne "Délai" est de type alphabétique. Les valeurs 10 et 100
  seront classées avant la valeur 20. Il suffit toutefois d'effectuer le tri sur
  la colonne "Échéance" pour obtenir le résultat souhaité ;
- un tri sur la colonne priorité positionne les tâches sans priorité avant les
  tâches possédant la prorité `(A)`. 

### Filtre

Plusieurs filtres sont facilement activables. Ils sont cumulatifs et permettent,
par exemple, de n'afficher les tâches n'appartenant qu'à tel projet *et*
appartenant à tel contexte. Un champ texte permet également de filtrer selon une
chaîne de caractères.

![Filtrage des tâches](img/sisyphe-filtre.gif)

La liste permettant de filtrer par priorité est affublée d'un bouton à cocher
intitulé "Priorités supérieures". S'il est coché, alors les tâches ayant la
priorité sélectionnée *mais aussi les tâches ayant une priorité plus forte*
seront affichées.

Par défaut, Sisyphe n'applique aucun filtre.

### Coloration des délais

Afin de mettre en évidence les tâches à exécutées urgemment, l'utilisatrice peut
définir jusqu'à trois règles de coloration des délais (*via* le menu `Fichier`
puis `Préférences`).

![Délais colorés](img/sisyphe-couleurs.png)


## Nouveautés

### Version 2020-12-04

- L'utilisatrice peut personnaliser la couleur des délais en fonction de la
  durée de ceux-ci (trois règles sont éditables)

## À venir

Sisyphe est un logiciel jeune, en rapide évolution. Dans ma *todo list* se
trouvent :

- l'ajout "d'action" par tâche (pour ouvrir une page web ou lancer un logiciel,
  par exemple) ;
- ~~la coloration personnablisable des tâches (couleur du texte, couleur de fond)
  en fonction du délai ;~~ (ver. 2020-12-04)
- la possibilité de renseigner un projet et un contexte par défaut, qui
  s'appliqueraient aux tâches qui sont saisies sans ces informations ;
- la possibilité de ranger Sisyphe dans le systray ;
- la possibilité "d'annuler" une tâche : elle serait archivée dans le fichier
  `done.txt`, mais précédée d'une mention personnablisable (comme `ANNULÉE`)
- l'auto-complétion des contextes et projets lors de la saisie d'une nouvelle
  tâche.

N'étant pas du tout un codeur chevronné, ces quelques points ne constituent
pas des promesses ;-)

Je suis bien sûr très attentif aux suggestions qui pourraient m'être faites.

## Comment bien utiliser un tel logiciel de gestions de tâches ?

Ce qui suit vise moins à décrire le logiciel, qu'à proposer une façon de
l'utiliser. Cela n'exprime que mon point de vue et chacune doit adapter l'usage à
ses besoins, ses préférences, ses routines… Ce point de vue est forgé par la
lecture de plusieurs documents, notamment le fameux ouvrage *Getting things
done* ayant donné naissance à la méthode GTD, mais également à d'autres sources,
comme la méthode PARA.

1. ne considérez pas une priorité comme une donnée absolue, elle est contextuelle.
   Ne perdez pas de temps à vous demander si une tâche est par essence plus ou
   moins importante que telle autre tâche. Car même si une tâche
   ne concerne pas une activité critique de votre travail, elle devra être
   réalisée… La priorité reflète donc moins "l'importance intrinsèque" d'une
   tâche que la place qu'elle doit occuper dans votre flux de travail. Cette
   place étant mouvante (selon le délai restant, la charge globale de travail,
   votre humeur, votre énergie…) la priorité est une donnée elle-même mouvante, pour ne pas
   dire volatile. C'est pourquoi Sisyphe offre la possiblité de modifier une
   priorité si facilement ;-) Une stratégie peut être, par exemple, d'appliquer
   la priorité `(A)` aux tâches devant être effectuées le jour même, `(B)` aux
   tâches devant être réalisées dans les deux jours, etc. Cette classification
   mériterait ainsi d'être produite chaque matin, voire idéalement la veille au
   soir ;
2. utilisez ni trop, ni trop peu de projets. À mon sens, l'intérêt de renseigner
   le projet auquel une tâche appartient est de pouvoir se concentrer, pendant 1
   heure ou une demi-journée, sur un même projet sans s'éparpiller. L'outil
   filtre est très utile pour cela. Si vous utilisez *trop* de projets, vous
   prenez le risque de n'avoir que peu de tâches par projet et donc de devoir
   fréquemment manipuler le logiciel ; à l'inverse, regrouper de nombreuses tâches
   en ayant que *trop peu* de projets n'aide pas à la lecture et l'appréhension
   de celles-ci. Ce conseil signifie donc que vous allez qualifier de "projet"
   des "blocs de tâches" qui ne vont pas être perçus comme un "projet" par des
   collègues ou qui ne collent pas à la notion de "projet" telle qu'elle
   pourrait être perçue de l'extérieure ("construire une centrale nucléaire" est
   un projet qui regrouperait un trop grand nombre de tâches…) ;
3. utilisez les contextes pour indiquer le statut de la tâche, et abandonnez la
   définition originelle du "contexte". La notion de "contexte" est liée, dans
   la démarche GTD, au cadre matériel permettant ou non de réaliser telle ou telle
   tâche. Si vous travaillez essentiellement avec un ordinateur et un téléphone,
   cette dimension matérielle perd de sa pertinence car vous pouvez à peu près
   effectuer les mêmes tâches depuis chez vous, au bureau ou dans le train. Pour
   ma part, j'utilise donc le contexte pour indiquer le statut d'une tâche :
   - `@todo` pour les tâches "activables", c'est-à-dire pouvant immédiatement
     être produites (ou au moins, entamées) ;
   - `@encours` pour les tâches, souvent longues, en train d'être produites. Il
     est utile de les marquer de la sorte afin de les retrouver aisément si l'on
     a été distrait par une sollicitation externe ;
   - `@backlog` pour les tâches non activables, c'est-à-dire qui ne peuvent pas
     être entamées (parce que vous attendez une information, parce qu'elles ne
     devront être réalisées que dans 6 mois, etc.) Les prendre ainsi en note
     permet simplement de ne pas les oublier !
   - `@veille` pour les tâches qui nécessitent de rester en veille : surveiller
     l'évolution d'un sondage en ligne, surveiller la réponse à un courriel… La
     veille n'est pas passive et peut au contraire nécessiter une action : la
     relance.
4. simplifiez-vous la vie en fixant une date d'échéance qui vous rappelera quand
   effectuer une action. En ce sens, la date d'échéance n'est pas seulement la
   date à laquelle une tâche doit *obligatoirement* être réalisée, mais une
   tâche pour laquelle il serait *judicieux* d'entreprendre une action à telle
   moment. C'est particulièrement vrai pour les tâches de type `@veille` : pour
   ma part, si je dois suivre la réponse à un courriel, je fixe une date
   d'échéance de 2 jours si j'estime la réponse urgente et importante et de
   5 jours sinon. Ce délai écoulé, je relance mon interlocutrice.
5. ayez toujours Sisyphe, ou un autre logiciel, à portée de main. À mon sens,
   l'étape la plus difficile de la démarche GTD est la première : la collecte.
   Collocter l'ensemble des tâches à effectuer est en effet un travail bien plus
   dur qu'il n'y paraît, pour tout un tas de raison : des idées de tâches
   peuvent venir n'importe quand, n'importe où ; il peut paraître rébarbatif de
   toutes les saisir, sans arrêt ; certaines semblent tellement évidentes qu'on
   ne les voit même plus comme étant des tâches, etc. Une façon de limiter ces
   difficultés est de limiter au maximum le "frottement" qui consiste à se
   saisir d'une app pour y noter une tâche. Donc idéalement, gardez Sisyphe
   ouvert tout le temps, et sur tous vos bureaux.

   [jln](https://mastodon.zaclys.com/@jln)
