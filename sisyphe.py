import os.path

from PySide2 import QtCore, QtGui, QtWidgets

from gui.delay_coloration import Ui_Form
from gui.gui2 import Ui_MainWindow

# from datetime import timedelta
from task import Task
from tasks import Tasks

colors = {
    "Blanc": "white",
    "Bleu": "blue",
    "Jaune": "yellow",
    "Noir": "black",
    "Orange": "orange",
    "Rose": "pink",
    "Rouge": "red",
    "Vert": "green",
    "Violet": "violet",
}


class WinDelay(QtWidgets.QDialog, Ui_Form):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.setup_connection()

    def setup_connection(self):
        # self.buttonBox.accepted.connect(self.accept)
        # self.buttonBox.rejected.connect(self.reject)
        self.buttonBox.clicked.connect(self.btn_clicked)
        self.btn_txt_1.clicked.connect(
            lambda: self.show_colorpicker(self.textcolor_d1)
        )
        self.btn_txt_2.clicked.connect(
            lambda: self.show_colorpicker(self.textcolor_d2)
        )
        self.btn_txt_3.clicked.connect(
            lambda: self.show_colorpicker(self.textcolor_d3)
        )
        self.btn_back_1.clicked.connect(
            lambda: self.show_colorpicker(self.backcolor_d1)
        )
        self.btn_back_2.clicked.connect(
            lambda: self.show_colorpicker(self.backcolor_d2)
        )
        self.btn_back_3.clicked.connect(
            lambda: self.show_colorpicker(self.backcolor_d3)
        )

    def btn_clicked(self, btn):
        """Fonction connectée à la buttonBox
        Appelle une fx selon le bouton cliqué
        """
        role = self.buttonBox.buttonRole(btn)

        if role == QtWidgets.QDialogButtonBox.ResetRole:
            # Clic sur reset : on réinitialise le formulaire
            self.spin_d1.clear()
            self.spin_d2.clear()
            self.spin_d3.clear()
            self.textcolor_d1.setText("")
            self.textcolor_d2.setText("")
            self.textcolor_d3.setText("")
            self.backcolor_d1.setText("")
            self.backcolor_d2.setText("")
            self.backcolor_d3.setText("")
            self.textcolor_d1.setStyleSheet("background-color: none")
            self.textcolor_d2.setStyleSheet("background-color: none")
            self.textcolor_d3.setStyleSheet("background-color: none")
            self.backcolor_d1.setStyleSheet("background-color: none")
            self.backcolor_d2.setStyleSheet("background-color: none")
            self.backcolor_d3.setStyleSheet("background-color: none")
        elif role == QtWidgets.QDialogButtonBox.RejectRole:
            self.action = "cancel"
            self.close()
        elif role == QtWidgets.QDialogButtonBox.AcceptRole:
            self.action = "ok"
            self.close()

    def action_ok(self):
        self.close()

    def show_colorpicker(self, destination):
        """Affiche le dialog pour la sélection d'une couleur dans la palette.
        La fonction renseigne ensuite la valeur de la couleur choisie dans le
        champ texte passé en argument
        """
        initial = destination.text() if destination.text() else ""
        color_dialog = QtWidgets.QColorDialog()
        new_color = color_dialog.getColor(
            initial=initial, options=QtWidgets.QColorDialog.ShowAlphaChannel
        )
        # Qcolor.HexArgb permet de récupérer la valeur du canal alpha
        if new_color:
            new_color = new_color.name(QtGui.QColor.HexArgb)
            destination.setStyleSheet("background-color: " + new_color)
            destination.setText(new_color)


class App(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()

        self.settings = QtCore.QSettings("Sisyphe", "App1")
        self.setupUi(self)

        if self.settings.value("working_path"):
            self.load_tasks()
        else:
            self.dlg_open()

        self.setup_connection()
        self.setup_shortcuts()
        self.populate_cb_filter_context()
        self.populate_cb_filter_project()
        self.populate_cb_filter_priority()
        self.populate_tasks()
        self.tree_tasks.sortItems(0, QtCore.Qt.AscendingOrder)
        self.win = None

        try:
            self.resize(self.settings.value("windows_size"))
            self.move(self.settings.value("windows_pos"))
            self.tree_tasks.setColumnWidth(
                0, int(self.settings.value("column_priority_size"))
            )
            self.tree_tasks.setColumnWidth(
                1, int(self.settings.value("column_creadate_size"))
            )
            self.tree_tasks.setColumnWidth(
                2, int(self.settings.value("column_body_size"))
            )
            self.tree_tasks.setColumnWidth(
                3, int(self.settings.value("column_duedate_size"))
            )
            self.tree_tasks.setColumnWidth(
                4, int(self.settings.value("column_delay_size"))
            )
        except:
            pass

    def setup_connection(self):
        """Relie les interactions graphiques aux méthodes qu'elles doivent
        appeler.
        """
        self.le_new_task.returnPressed.connect(self.add_task)
        self.tree_tasks.itemChanged.connect(self.update_task)
        self.le_filter.returnPressed.connect(self.populate_tasks)
        self.le_filter.textEdited.connect(self.populate_tasks)
        self.cb_filter_context.currentIndexChanged.connect(
            self.filter_context_change
        )
        self.cb_filter_project.currentIndexChanged.connect(
            self.filter_project_change
        )
        self.cb_filter_priority.currentIndexChanged.connect(
            self.filter_priority_change
        )
        self.cbox_priority.stateChanged.connect(self.populate_tasks)
        self.btn_clear_filter.clicked.connect(self.clear_filter)
        self.tree_tasks.blockSignals(True)
        # Menu
        self.actionOuvrir.triggered.connect(self.dlg_open)
        self.actionPref.triggered.connect(self.dlg_delay)

    def setup_shortcuts(self):
        """Attribution des raccourcis clavier aux fonctions"""
        QtWidgets.QShortcut(QtGui.QKeySequence("x"), self, self.archive_task)
        QtWidgets.QShortcut(
            QtGui.QKeySequence("-"), self, lambda: self.update_priority("-1")
        )
        QtWidgets.QShortcut(
            QtGui.QKeySequence("+"), self, lambda: self.update_priority("1")
        )
        QtWidgets.QShortcut(
            QtGui.QKeySequence(">"), self, lambda: self.update_duedate(1)
        )
        QtWidgets.QShortcut(
            QtGui.QKeySequence("<"), self, lambda: self.update_duedate(-1)
        )

    def _create_files_if_not_exist(self):
        """Vérifie l'existence des fichiers todo.txt et done.txt et les crée le
        cas échéant
        """
        try:
            if not os.path.exists(self.todofile):
                open(self.todofile, "w").close()
            if not os.path.exists(self.archivesfile):
                open(self.archivesfile, "w").close()
        except FileNotFoundError:
            self.dlg_open()

    def load_tasks(self):
        """Charge les tâches depuis l'emplacement enregistré par QSettings"""
        self._set_todofilepath(self.settings.value("working_path"))
        self.archivesfile = os.path.join(
            self.settings.value("working_path"), "done.txt"
        )
        self._create_files_if_not_exist()
        self.tasks = Tasks(self.todofile, self.archivesfile)
        self.tasks.read_todofile()
        self.tasks.read_archivesfile()

    def dlg_open(self):
        """Ouvre une boîte de dialogue de type QFileDialog pour définir le
        répertoire de travail et créer les fichiers todo.txt et done.txt si
        besoin
        """
        working_path = QtWidgets.QFileDialog.getExistingDirectory(
            self,
            "Emplacement des fichiers todo.txt et done.txt",
            "/home",
            QtWidgets.QFileDialog.ShowDirsOnly,
        )
        if working_path:
            self._set_todofilepath(working_path)
            self.archivesfile = os.path.join(working_path, "done.txt")
            self._create_files_if_not_exist()
            self.tasks = Tasks(self.todofile, self.archivesfile)
            self.tasks.read_todofile()
            self.tasks.read_archivesfile()

            self.settings.setValue("working_path", working_path)
            self.populate_cb_filter_context()
            self.populate_cb_filter_project()
            self.populate_cb_filter_priority()
            self.populate_tasks()

        # Si l'utilisateur clique sur "annuler" alors qu'aucun dossier de
        # travail n'est défini, il faut le forcer à en définir un
        elif not self.settings.value("working_path"):
            dlg_err = QtWidgets.QMessageBox()
            dlg_err.setIcon(QtWidgets.QMessageBox.Critical)
            dlg_err.setModal(True)
            dlg_err.setText("Vous DEVEZ sélectionner un répertoire de travail")
            dlg_err.setWindowTitle("Erreur")
            feedback = dlg_err.exec_()
            if feedback:
                self.dlg_open()

    def _set_todofilepath(self, working_path):
        self.todofile = os.path.join(working_path, "todo.txt")

    def dlg_delay(self):
        win = WinDelay()
        win.setModal(True)
        color_fields = [
            "textcolor_d1",
            "backcolor_d1",
            "textcolor_d2",
            "backcolor_d2",
            "textcolor_d3",
            "backcolor_d3",
        ]

        self.settings.beginGroup("color_delay")
        # Remplissage des champs si les couleurs sont pré-existantes dans
        # QSettings.
        for color in color_fields:
            try:
                if self.settings.value(color):
                    field = getattr(win, color)
                    field.setText(self.settings.value(color))
                    field.setStyleSheet(
                        "background-color: " + self.settings.value(color)
                    )
            except AttributeError:
                pass
        # Remplissage des champs si les délais sont pré-existants dans
        # QSettings
        for d in ["d1", "d2", "d3"]:
            if self.settings.value(d):
                field = getattr(win, "spin_" + d)
                field.setValue(int(self.settings.value(d)))
        self.settings.endGroup()

        win.exec_()

        if hasattr(win, "action") and win.action == "ok":
            # Si clic sur ok, enregistrement des valeurs du formulaire dans
            # qsettings
            self.settings.beginGroup("color_delay")
            for color in color_fields:
                field = getattr(win, color)
                colorname = field.text()
                if colorname:
                    self.settings.setValue(color, colorname)
                else:
                    self.settings.remove(color)

            for d in ["d1", "d2", "d3"]:
                field = getattr(win, "spin_" + d)
                days = field.value()
                self.settings.setValue(d, days)

            self.settings.endGroup()
            self.populate_tasks()

    def clear_filter(self):
        self.le_filter.clear()
        self.cb_filter_context.setCurrentIndex(-1)
        self.cb_filter_project.setCurrentIndex(-1)
        self.cb_filter_priority.setCurrentIndex(-1)
        self.populate_tasks()

    def _filter_by_body(self):
        filter = self.le_filter.text()
        filtered_tasks = []
        for task in self.tasks.filter_by_body(filter):
            filtered_tasks.append(task)
        return filtered_tasks

    def _filter_by_context(self):
        filter = self.cb_filter_context.currentText()
        filtered_tasks = []
        for task in self.tasks.filter_by_context(filter):
            filtered_tasks.append(task)
        return filtered_tasks

    def _filter_by_priority(self, mode="sup"):
        filter = self.cb_filter_priority.currentText()
        filtered_tasks = []
        if mode == "sup":
            for task in self.tasks.filter_by_priority(filter, mode="sup"):
                filtered_tasks.append(task)
        elif mode == "only":
            for task in self.tasks.filter_by_priority(filter, mode="only"):
                filtered_tasks.append(task)
        return filtered_tasks

    def _filter_by_project(self):
        filter = self.cb_filter_project.currentText()
        filtered_tasks = []
        for task in self.tasks.filter_by_project(filter):
            filtered_tasks.append(task)
        return filtered_tasks

    def populate_cb_filter_context(self):
        """Fonction remplissant le combobox avec l'ensemble des contextes
        existants
        """
        self.cb_filter_context.clear()
        self.cb_filter_context.addItem("")
        all_contexts = self.tasks.get_all_contexts(task_status="not_done")
        for context in all_contexts:
            self.cb_filter_context.addItem(context)

    def populate_cb_filter_priority(self):
        self.cb_filter_priority.clear()
        self.cb_filter_priority.addItem("")
        priority = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        for letter in priority:
            letter = f"({letter})"
            self.cb_filter_priority.addItem(letter)

    def populate_cb_filter_project(self):
        self.cb_filter_project.clear()
        self.cb_filter_project.addItem("")
        all_projects = self.tasks.get_all_projects(task_status="not_done")
        for project in all_projects:
            self.cb_filter_project.addItem(project)

    def filter_context_change(self):
        if self.cb_filter_context.currentIndex() == 0:
            self.cb_filter_context.setCurrentIndex(-1)
        self.populate_tasks()

    def filter_priority_change(self):
        if self.cb_filter_priority.currentIndex() == 0:
            self.cb_filter_priority.setCurrentIndex(-1)
        self.populate_tasks()

    def filter_project_change(self):
        if self.cb_filter_project.currentIndex() == 0:
            self.cb_filter_project.setCurrentIndex(-1)
        self.populate_tasks()

    def populate_tasks(self):
        self.tree_tasks.blockSignals(True)
        while self.tree_tasks.topLevelItemCount() > 0:
            self.tree_tasks.takeTopLevelItem(0)
        tasks_set = set()
        for task in self.tasks.get_tasks():
            tasks_set.add(task)

        if self.le_filter.text() != "":
            filtered_tasks_body = set(self._filter_by_body())
            tasks_set = tasks_set.intersection(filtered_tasks_body)

        if self.cb_filter_priority.currentIndex() != -1:
            if self.cbox_priority.checkState():
                filtered_tasks_pri = set(self._filter_by_priority(mode="sup"))
            else:
                filtered_tasks_pri = set(self._filter_by_priority(mode="only"))
            tasks_set = tasks_set.intersection(filtered_tasks_pri)

        if self.cb_filter_context.currentIndex() != -1:
            filtered_tasks_context = set(self._filter_by_context())
            tasks_set = tasks_set.intersection(filtered_tasks_context)

        if self.cb_filter_project.currentIndex() != -1:
            filtered_tasks_project = set(self._filter_by_project())
            tasks_set = tasks_set.intersection(filtered_tasks_project)

        tasks_list = list(tasks_set)

        for task in tasks_list:
            tree_item = QtWidgets.QTreeWidgetItem(self.tree_tasks)
            if task.duedate:
                tree_item.setText(3, str(task.duedate).rstrip())
                tree_item.setText(4, str(task.delay).rstrip())
                # body = task.replace(f"due:{task.duedate}", "")
            # else:
            #     body = task
            tree_item.setText(0, task.priority)
            tree_item.setText(1, task.creation_date)
            tree_item.setText(2, task.body.rstrip())

            tree_item.setFlags(tree_item.flags() | QtCore.Qt.ItemIsEditable)
            tree_item.setData(2, QtCore.Qt.UserRole, task)

            # couleur selon le delai
            # self.settings.beginGroup("color_delay")
            # if task.delay:
            #     if int(task.delay) <= int(self.settings.value("d1")):
            #         tree_item.setTextColor(
            #              4, self.settings.value("textcolor_d1")
            #          )
            #         tree_item.setBackgroundColor(
            #             4, self.settings.value("backcolor_d1")
            #         )
            #     elif int(task.delay) <= int(self.settings.value("d2")):
            #         tree_item.setTextColor(
            #             4, self.settings.value("textcolor_d2")
            #         )
            #         tree_item.setBackgroundColor(
            #             4, self.settings.value("backcolor_d2")
            #         )
            #     elif int(task.delay) <= int(self.settings.value("d3")):
            #         tree_item.setTextColor(
            #             4, self.settings.value("textcolor_d3")
            #         )
            #         tree_item.setBackgroundColor(
            #             4, self.settings.value("backcolor_d3")
            #         )
            self.color_items()

            # self.settings.endGroup()
        self.tree_tasks.blockSignals(False)

    def color_items(self):
        """Parcourt les items du QTreeeWidget pour les colorier selon les
        règles établies
        """
        self.settings.beginGroup("color_delay")
        delay1 = (
            int(self.settings.value("d1")) if self.settings.value("d1") else ""
        )
        delay2 = (
            int(self.settings.value("d2")) if self.settings.value("d2") else ""
        )
        delay3 = (
            int(self.settings.value("d3")) if self.settings.value("d3") else ""
        )
        backcolor_d1 = backcolor_d2 = backcolor_d3 = None
        textcolor_d1 = textcolor_d2 = textcolor_d3 = None
        # TODO: réduire ce passage
        if (
            self.settings.value("backcolor_d1")
            and self.settings.value("backcolor_d1") != ""
        ):
            backcolor_d1 = self.settings.value("backcolor_d1")

        if (
            self.settings.value("backcolor_d2")
            and self.settings.value("backcolor_d2") != ""
        ):
            backcolor_d2 = self.settings.value("backcolor_d2")

        if (
            self.settings.value("backcolor_d3")
            and self.settings.value("backcolor_d3") != ""
        ):
            backcolor_d3 = self.settings.value("backcolor_d3")

        if (
            self.settings.value("textcolor_d1")
            and self.settings.value("textcolor_d1") != ""
        ):
            textcolor_d1 = self.settings.value("textcolor_d1")

        if (
            self.settings.value("textcolor_d2")
            and self.settings.value("textcolor_d2") != ""
        ):
            textcolor_d2 = self.settings.value("textcolor_d2")

        if (
            self.settings.value("textcolor_d3")
            and self.settings.value("textcolor_d3") != ""
        ):
            textcolor_d3 = self.settings.value("textcolor_d3")

        root = self.tree_tasks.invisibleRootItem()
        nb_items = root.childCount()

        for i in range(nb_items):
            item = root.child(i)
            delay = item.data(2, QtCore.Qt.UserRole).delay

            if type(delay) == int:
                if delay1 != "" and delay <= delay1:
                    if backcolor_d1:
                        backcolor_d1 = QtGui.QColor(backcolor_d1)
                        item.setBackgroundColor(4, backcolor_d1)
                    if textcolor_d1:
                        textcolor_d1 = QtGui.QColor(textcolor_d1)
                        item.setForeground(4, textcolor_d1)
                elif delay2 != "" and delay <= delay2:
                    if backcolor_d2:
                        backcolor_d2 = QtGui.QColor(backcolor_d2)
                        item.setBackgroundColor(4, backcolor_d2)
                    if textcolor_d2:
                        textcolor_d2 = QtGui.QColor(textcolor_d2)
                        item.setTextColor(4, textcolor_d2)
                elif delay3 != "" and delay <= delay3:
                    if backcolor_d3:
                        backcolor_d3 = QtGui.QColor(backcolor_d3)
                        item.setBackgroundColor(4, backcolor_d3)
                    if textcolor_d3:
                        textcolor_d3 = QtGui.QColor(textcolor_d3)
                        item.setTextColor(4, textcolor_d3)

        self.settings.endGroup()

    def add_task(self):
        new_task_content = self.le_new_task.text()
        if not new_task_content:
            return False
        new_task = Task(new_task_content)
        new_task.set_creation_date()
        self.tasks.append(new_task)
        self.tasks.order_by_pri()
        self.le_new_task.setText("")
        self.write_tasks()
        #        self.tree_tasks.clear()
        self.populate_cb_filter_project()
        self.populate_cb_filter_context()
        self.populate_tasks()

    def archive_task(self):
        if self.tree_tasks.selectedItems():
            task_obj = self.tree_tasks.selectedItems()[0]
            task_obj.data(2, QtCore.Qt.UserRole).set_done()
            self.write_tasks()
            #            tasks.order_by_pri()
            #            self.tree_tasks.clear()
            self.populate_tasks()

    def update_task(self, item, column):
        """Fonction exécutée lors de la modification directe d'un
        QTreeWidgetItem
        La fonction met à jour la tâche éditée

        Deux cas de figure
        - soit le nouveau contenu débute par "x " et il faut alors passer la
        tâche "done"
        - soit pas, et il faut alors simplement la mettre à jour
        """
        duedate = f" due:{item.text(3)}" if item.text(3) else ""
        # new_task = f"{item.text(0)} {item.text(1)} {item.text(2)}{duedate}"
        priority = item.text(0) + " " if item.text(0) else ""
        creation_date = item.text(1) + " " if item.text(1) else ""
        body = item.text(2).strip()
        new_task = f"{priority}{creation_date}{body}{duedate}"
        if item.text(0).startswith("x ") or item.text(2).startswith("x "):
            item.data(2, QtCore.Qt.UserRole).set_done()
        else:
            item.data(2, QtCore.Qt.UserRole).set_content(new_task)
        self.write_tasks()
        self.tasks.order_by_pri()
        #        self.tree_tasks.clear()
        self.populate_cb_filter_project()
        self.populate_cb_filter_context()
        self.populate_tasks()

    def update_duedate(self, nb_days):
        if self.tree_tasks.selectedItems():
            selected_item = self.tree_tasks.selectedItems()[0]
            uuid = selected_item.data(2, QtCore.Qt.UserRole).uuid
            selected_item.data(2, QtCore.Qt.UserRole).update_duedate(nb_days)

            self.write_tasks()
            self.populate_tasks()
            self._select_item_in_tree_by_uuid(uuid)

    def update_priority(self, direction):
        direction = int(direction)
        if self.tree_tasks.selectedItems():
            selected_item = self.tree_tasks.selectedItems()[0]
            uuid = selected_item.data(2, QtCore.Qt.UserRole).uuid
            selected_item.data(2, QtCore.Qt.UserRole).update_priority(direction)
            # if direction == "1":
            #    selected_item.data(2, QtCore.Qt.UserRole).update_priority()
            # else:
            #    selected_item.data(2, QtCore.Qt.UserRole).down_priority()
            self.write_tasks()
            self.populate_tasks()
            self._select_item_in_tree_by_uuid(uuid)

    def write_tasks(self):
        self.tasks.write_todofile()
        self.tasks.write_archivesfile()

    def _select_item_in_tree_by_uuid(self, uuid):
        iterator = QtWidgets.QTreeWidgetItemIterator(self.tree_tasks)
        while iterator:
            if iterator.value().data(2, QtCore.Qt.UserRole).uuid == uuid:
                self.tree_tasks.setCurrentItem(iterator.value())
                break
            iterator += 1

    def closeEvent(self, event):
        self.settings.setValue("windows_size", self.size())
        self.settings.setValue("windows_pos", self.pos())
        self.settings.setValue(
            "column_priority_size", self.tree_tasks.columnWidth(0)
        )
        self.settings.setValue(
            "column_creadate_size", self.tree_tasks.columnWidth(1)
        )
        self.settings.setValue(
            "column_body_size", self.tree_tasks.columnWidth(2)
        )
        self.settings.setValue(
            "column_duedate_size", self.tree_tasks.columnWidth(3)
        )
        self.settings.setValue(
            "column_delay_size", self.tree_tasks.columnWidth(4)
        )


if __name__ == "__main__":
    app = QtWidgets.QApplication([])
    win = App()
    win.show()
    app.exec_()
