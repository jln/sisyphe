# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'delay_coloration.ui'
##
## Created by: Qt User Interface Compiler version 5.15.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(418, 590)
        self.gridLayout_4 = QGridLayout(Form)
        self.gridLayout_4.setObjectName(u"gridLayout_4")
        self.label = QLabel(Form)
        self.label.setObjectName(u"label")
        self.label.setScaledContents(False)
        self.label.setWordWrap(True)

        self.gridLayout_4.addWidget(self.label, 0, 0, 1, 2)

        self.buttonBox = QDialogButtonBox(Form)
        self.buttonBox.setObjectName(u"buttonBox")
        self.buttonBox.setStandardButtons(QDialogButtonBox.Abort|QDialogButtonBox.Ok|QDialogButtonBox.Reset)

        self.gridLayout_4.addWidget(self.buttonBox, 4, 0, 1, 1)

        self.groupBox = QGroupBox(Form)
        self.groupBox.setObjectName(u"groupBox")
        self.gridLayout = QGridLayout(self.groupBox)
        self.gridLayout.setObjectName(u"gridLayout")
        self.label_2 = QLabel(self.groupBox)
        self.label_2.setObjectName(u"label_2")

        self.gridLayout.addWidget(self.label_2, 0, 0, 1, 1)

        self.spin_d1 = QSpinBox(self.groupBox)
        self.spin_d1.setObjectName(u"spin_d1")
        self.spin_d1.setMinimum(-1000)
        self.spin_d1.setMaximum(1000)

        self.gridLayout.addWidget(self.spin_d1, 0, 1, 1, 2)

        self.label_3 = QLabel(self.groupBox)
        self.label_3.setObjectName(u"label_3")

        self.gridLayout.addWidget(self.label_3, 1, 0, 1, 1)

        self.textcolor_d1 = QLineEdit(self.groupBox)
        self.textcolor_d1.setObjectName(u"textcolor_d1")

        self.gridLayout.addWidget(self.textcolor_d1, 1, 1, 1, 1)

        self.btn_txt_1 = QPushButton(self.groupBox)
        self.btn_txt_1.setObjectName(u"btn_txt_1")

        self.gridLayout.addWidget(self.btn_txt_1, 1, 2, 1, 1)

        self.label_5 = QLabel(self.groupBox)
        self.label_5.setObjectName(u"label_5")

        self.gridLayout.addWidget(self.label_5, 2, 0, 1, 1)

        self.backcolor_d1 = QLineEdit(self.groupBox)
        self.backcolor_d1.setObjectName(u"backcolor_d1")

        self.gridLayout.addWidget(self.backcolor_d1, 2, 1, 1, 1)

        self.btn_back_1 = QPushButton(self.groupBox)
        self.btn_back_1.setObjectName(u"btn_back_1")

        self.gridLayout.addWidget(self.btn_back_1, 2, 2, 1, 1)


        self.gridLayout_4.addWidget(self.groupBox, 1, 0, 1, 2)

        self.groupBox_2 = QGroupBox(Form)
        self.groupBox_2.setObjectName(u"groupBox_2")
        self.gridLayout_2 = QGridLayout(self.groupBox_2)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.label_4 = QLabel(self.groupBox_2)
        self.label_4.setObjectName(u"label_4")

        self.gridLayout_2.addWidget(self.label_4, 0, 0, 1, 1)

        self.spin_d2 = QSpinBox(self.groupBox_2)
        self.spin_d2.setObjectName(u"spin_d2")
        self.spin_d2.setMinimum(-1000)
        self.spin_d2.setMaximum(1000)
        self.spin_d2.setValue(3)

        self.gridLayout_2.addWidget(self.spin_d2, 0, 1, 1, 2)

        self.label_6 = QLabel(self.groupBox_2)
        self.label_6.setObjectName(u"label_6")

        self.gridLayout_2.addWidget(self.label_6, 1, 0, 1, 1)

        self.textcolor_d2 = QLineEdit(self.groupBox_2)
        self.textcolor_d2.setObjectName(u"textcolor_d2")

        self.gridLayout_2.addWidget(self.textcolor_d2, 1, 1, 1, 1)

        self.btn_txt_2 = QPushButton(self.groupBox_2)
        self.btn_txt_2.setObjectName(u"btn_txt_2")

        self.gridLayout_2.addWidget(self.btn_txt_2, 1, 2, 1, 1)

        self.label_7 = QLabel(self.groupBox_2)
        self.label_7.setObjectName(u"label_7")

        self.gridLayout_2.addWidget(self.label_7, 2, 0, 1, 1)

        self.backcolor_d2 = QLineEdit(self.groupBox_2)
        self.backcolor_d2.setObjectName(u"backcolor_d2")

        self.gridLayout_2.addWidget(self.backcolor_d2, 2, 1, 1, 1)

        self.btn_back_2 = QPushButton(self.groupBox_2)
        self.btn_back_2.setObjectName(u"btn_back_2")

        self.gridLayout_2.addWidget(self.btn_back_2, 2, 2, 1, 1)


        self.gridLayout_4.addWidget(self.groupBox_2, 2, 0, 1, 2)

        self.groupBox_3 = QGroupBox(Form)
        self.groupBox_3.setObjectName(u"groupBox_3")
        self.gridLayout_3 = QGridLayout(self.groupBox_3)
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.label_8 = QLabel(self.groupBox_3)
        self.label_8.setObjectName(u"label_8")

        self.gridLayout_3.addWidget(self.label_8, 0, 0, 1, 1)

        self.spin_d3 = QSpinBox(self.groupBox_3)
        self.spin_d3.setObjectName(u"spin_d3")
        self.spin_d3.setEnabled(True)
        self.spin_d3.setFrame(True)
        self.spin_d3.setMinimum(-1000)
        self.spin_d3.setMaximum(1000)
        self.spin_d3.setValue(5)
        self.spin_d3.setDisplayIntegerBase(10)

        self.gridLayout_3.addWidget(self.spin_d3, 0, 1, 1, 2)

        self.label_9 = QLabel(self.groupBox_3)
        self.label_9.setObjectName(u"label_9")

        self.gridLayout_3.addWidget(self.label_9, 1, 0, 1, 1)

        self.textcolor_d3 = QLineEdit(self.groupBox_3)
        self.textcolor_d3.setObjectName(u"textcolor_d3")

        self.gridLayout_3.addWidget(self.textcolor_d3, 1, 1, 1, 1)

        self.btn_txt_3 = QPushButton(self.groupBox_3)
        self.btn_txt_3.setObjectName(u"btn_txt_3")

        self.gridLayout_3.addWidget(self.btn_txt_3, 1, 2, 1, 1)

        self.label_10 = QLabel(self.groupBox_3)
        self.label_10.setObjectName(u"label_10")

        self.gridLayout_3.addWidget(self.label_10, 2, 0, 1, 1)

        self.backcolor_d3 = QLineEdit(self.groupBox_3)
        self.backcolor_d3.setObjectName(u"backcolor_d3")

        self.gridLayout_3.addWidget(self.backcolor_d3, 2, 1, 1, 1)

        self.btn_back_3 = QPushButton(self.groupBox_3)
        self.btn_back_3.setObjectName(u"btn_back_3")

        self.gridLayout_3.addWidget(self.btn_back_3, 2, 2, 1, 1)


        self.gridLayout_4.addWidget(self.groupBox_3, 3, 0, 1, 2)


        self.retranslateUi(Form)

        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Coloration des d\u00e9lais", None))
        self.label.setText(QCoreApplication.translate("Form", u"Le niveau d'alerte 1 est le niveau d'alerte le plus \u00e9lev\u00e9. Un d\u00e9lai n\u00e9gatif signifie que l'\u00e9chance est d\u00e9j\u00e0 pass\u00e9e", None))
        self.groupBox.setTitle(QCoreApplication.translate("Form", u"Niveau d'alerte 1", None))
        self.label_2.setText(QCoreApplication.translate("Form", u"D\u00e9lai", None))
        self.spin_d1.setSuffix(QCoreApplication.translate("Form", u" j.", None))
        self.label_3.setText(QCoreApplication.translate("Form", u"Couleur du texte", None))
        self.btn_txt_1.setText(QCoreApplication.translate("Form", u"Palette", None))
        self.label_5.setText(QCoreApplication.translate("Form", u"Couleur du fond", None))
        self.btn_back_1.setText(QCoreApplication.translate("Form", u"Palette", None))
        self.groupBox_2.setTitle(QCoreApplication.translate("Form", u"Niveau d'alerte 2", None))
        self.label_4.setText(QCoreApplication.translate("Form", u"D\u00e9lai", None))
        self.spin_d2.setSuffix(QCoreApplication.translate("Form", u" j.", None))
        self.label_6.setText(QCoreApplication.translate("Form", u"Couleur du texte", None))
        self.btn_txt_2.setText(QCoreApplication.translate("Form", u"Palette", None))
        self.label_7.setText(QCoreApplication.translate("Form", u"Couleur du fond", None))
        self.btn_back_2.setText(QCoreApplication.translate("Form", u"Palette", None))
        self.groupBox_3.setTitle(QCoreApplication.translate("Form", u"Niveau d'alerte 3", None))
        self.label_8.setText(QCoreApplication.translate("Form", u"D\u00e9lai", None))
        self.spin_d3.setSpecialValueText("")
        self.spin_d3.setSuffix(QCoreApplication.translate("Form", u" j.", None))
        self.label_9.setText(QCoreApplication.translate("Form", u"Couleur du texte", None))
        self.btn_txt_3.setText(QCoreApplication.translate("Form", u"Palette", None))
        self.label_10.setText(QCoreApplication.translate("Form", u"Couleur du fond", None))
        self.btn_back_3.setText(QCoreApplication.translate("Form", u"Palette", None))
    # retranslateUi

