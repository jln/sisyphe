# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'gui.ui'
##
## Created by: Qt User Interface Compiler version 5.15.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(950, 462)
        self.gridLayout = QGridLayout(Form)
        self.gridLayout.setObjectName(u"gridLayout")
        self.groupBox_2 = QGroupBox(Form)
        self.groupBox_2.setObjectName(u"groupBox_2")
        self.groupBox_2.setFlat(False)
        self.horizontalLayout_2 = QHBoxLayout(self.groupBox_2)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.comboBox = QComboBox(self.groupBox_2)
        self.comboBox.setObjectName(u"comboBox")

        self.horizontalLayout_2.addWidget(self.comboBox)

        self.le_filter = QLineEdit(self.groupBox_2)
        self.le_filter.setObjectName(u"le_filter")

        self.horizontalLayout_2.addWidget(self.le_filter)

        self.cb_filter_context = QComboBox(self.groupBox_2)
        self.cb_filter_context.setObjectName(u"cb_filter_context")

        self.horizontalLayout_2.addWidget(self.cb_filter_context)

        self.cb_filter_project = QComboBox(self.groupBox_2)
        self.cb_filter_project.setObjectName(u"cb_filter_project")
        self.cb_filter_project.setFrame(True)
        self.cb_filter_project.setModelColumn(0)

        self.horizontalLayout_2.addWidget(self.cb_filter_project)

        self.btn_clear_filter = QPushButton(self.groupBox_2)
        self.btn_clear_filter.setObjectName(u"btn_clear_filter")

        self.horizontalLayout_2.addWidget(self.btn_clear_filter)


        self.gridLayout.addWidget(self.groupBox_2, 1, 1, 1, 2)

        self.btn_add_task = QPushButton(Form)
        self.btn_add_task.setObjectName(u"btn_add_task")

        self.gridLayout.addWidget(self.btn_add_task, 0, 2, 1, 1)

        self.le_new_task = QLineEdit(Form)
        self.le_new_task.setObjectName(u"le_new_task")
        self.le_new_task.setClearButtonEnabled(True)

        self.gridLayout.addWidget(self.le_new_task, 0, 0, 1, 2)

        self.groupBox = QGroupBox(Form)
        self.groupBox.setObjectName(u"groupBox")
        self.groupBox.setContextMenuPolicy(Qt.NoContextMenu)
        self.groupBox.setFlat(False)
        self.horizontalLayout = QHBoxLayout(self.groupBox)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.btn_order_pri = QPushButton(self.groupBox)
        self.btn_order_pri.setObjectName(u"btn_order_pri")

        self.horizontalLayout.addWidget(self.btn_order_pri)

        self.btn_order_due = QPushButton(self.groupBox)
        self.btn_order_due.setObjectName(u"btn_order_due")

        self.horizontalLayout.addWidget(self.btn_order_due)

        self.btn_order_crea = QPushButton(self.groupBox)
        self.btn_order_crea.setObjectName(u"btn_order_crea")

        self.horizontalLayout.addWidget(self.btn_order_crea)


        self.gridLayout.addWidget(self.groupBox, 1, 0, 1, 1)

        self.ls_tasks = QListWidget(Form)
        self.ls_tasks.setObjectName(u"ls_tasks")
        self.ls_tasks.setContextMenuPolicy(Qt.NoContextMenu)

        self.gridLayout.addWidget(self.ls_tasks, 2, 0, 1, 3)


        self.retranslateUi(Form)

        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Form", None))
        self.groupBox_2.setTitle(QCoreApplication.translate("Form", u"Filtre", None))
        self.le_filter.setPlaceholderText(QCoreApplication.translate("Form", u"Filtre libre", None))
        self.cb_filter_context.setPlaceholderText(QCoreApplication.translate("Form", u"@contexte", None))
        self.cb_filter_project.setCurrentText(QCoreApplication.translate("Form", u"+projet", None))
        self.cb_filter_project.setPlaceholderText(QCoreApplication.translate("Form", u"+projet", None))
        self.btn_clear_filter.setText(QCoreApplication.translate("Form", u"R\u00e9initialiser", None))
        self.btn_add_task.setText(QCoreApplication.translate("Form", u"Ajouter", None))
        self.le_new_task.setInputMask("")
        self.le_new_task.setPlaceholderText(QCoreApplication.translate("Form", u"Nouvelle t\u00e2che", None))
        self.groupBox.setTitle(QCoreApplication.translate("Form", u"Tri", None))
        self.btn_order_pri.setText(QCoreApplication.translate("Form", u"Priorit\u00e9", None))
        self.btn_order_due.setText(QCoreApplication.translate("Form", u"\u00c9ch\u00e9ance", None))
        self.btn_order_crea.setText(QCoreApplication.translate("Form", u"Cr\u00e9ation", None))
    # retranslateUi

