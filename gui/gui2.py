# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'gui2.ui'
##
## Created by: Qt User Interface Compiler version 5.15.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(800, 459)
        self.actionOuvrir = QAction(MainWindow)
        self.actionOuvrir.setObjectName(u"actionOuvrir")
        self.actionQuitter = QAction(MainWindow)
        self.actionQuitter.setObjectName(u"actionQuitter")
        self.actionShowTasksWoCont = QAction(MainWindow)
        self.actionShowTasksWoCont.setObjectName(u"actionShowTasksWoCont")
        self.actionShowTasksWoPro = QAction(MainWindow)
        self.actionShowTasksWoPro.setObjectName(u"actionShowTasksWoPro")
        self.actionPref = QAction(MainWindow)
        self.actionPref.setObjectName(u"actionPref")
        self.actionR_initialiser = QAction(MainWindow)
        self.actionR_initialiser.setObjectName(u"actionR_initialiser")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.gridLayout_2 = QGridLayout(self.centralwidget)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.tree_tasks = QTreeWidget(self.centralwidget)
        self.tree_tasks.setObjectName(u"tree_tasks")
        self.tree_tasks.setRootIsDecorated(True)
        self.tree_tasks.setSortingEnabled(True)
        self.tree_tasks.setHeaderHidden(False)

        self.gridLayout_2.addWidget(self.tree_tasks, 1, 0, 1, 2)

        self.le_new_task = QLineEdit(self.centralwidget)
        self.le_new_task.setObjectName(u"le_new_task")

        self.gridLayout_2.addWidget(self.le_new_task, 0, 0, 1, 1)

        self.btn_add_task = QPushButton(self.centralwidget)
        self.btn_add_task.setObjectName(u"btn_add_task")

        self.gridLayout_2.addWidget(self.btn_add_task, 0, 1, 1, 1)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 800, 29))
        self.menuFichiers = QMenu(self.menubar)
        self.menuFichiers.setObjectName(u"menuFichiers")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.dockWidget = QDockWidget(MainWindow)
        self.dockWidget.setObjectName(u"dockWidget")
        self.dockWidgetContents = QWidget()
        self.dockWidgetContents.setObjectName(u"dockWidgetContents")
        self.gridLayout = QGridLayout(self.dockWidgetContents)
        self.gridLayout.setObjectName(u"gridLayout")
        self.frame = QFrame(self.dockWidgetContents)
        self.frame.setObjectName(u"frame")
        self.verticalLayout_2 = QVBoxLayout(self.frame)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.le_filter = QLineEdit(self.frame)
        self.le_filter.setObjectName(u"le_filter")

        self.verticalLayout_2.addWidget(self.le_filter)

        self.cb_filter_priority = QComboBox(self.frame)
        self.cb_filter_priority.setObjectName(u"cb_filter_priority")

        self.verticalLayout_2.addWidget(self.cb_filter_priority)

        self.cbox_priority = QCheckBox(self.frame)
        self.cbox_priority.setObjectName(u"cbox_priority")

        self.verticalLayout_2.addWidget(self.cbox_priority)

        self.cb_filter_context = QComboBox(self.frame)
        self.cb_filter_context.setObjectName(u"cb_filter_context")

        self.verticalLayout_2.addWidget(self.cb_filter_context)

        self.cb_filter_project = QComboBox(self.frame)
        self.cb_filter_project.setObjectName(u"cb_filter_project")

        self.verticalLayout_2.addWidget(self.cb_filter_project)

        self.btn_clear_filter = QPushButton(self.frame)
        self.btn_clear_filter.setObjectName(u"btn_clear_filter")

        self.verticalLayout_2.addWidget(self.btn_clear_filter)


        self.gridLayout.addWidget(self.frame, 0, 0, 1, 2)

        self.dockWidget.setWidget(self.dockWidgetContents)
        MainWindow.addDockWidget(Qt.RightDockWidgetArea, self.dockWidget)

        self.menubar.addAction(self.menuFichiers.menuAction())
        self.menuFichiers.addAction(self.actionOuvrir)
        self.menuFichiers.addAction(self.actionPref)
        self.menuFichiers.addSeparator()
        self.menuFichiers.addAction(self.actionQuitter)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"Sisyphe", None))
        self.actionOuvrir.setText(QCoreApplication.translate("MainWindow", u"Ouvrir", None))
#if QT_CONFIG(statustip)
        self.actionOuvrir.setStatusTip(QCoreApplication.translate("MainWindow", u"S\u00e9lectionner l'emplacement des fichiers todo.txt et done.txt", None))
#endif // QT_CONFIG(statustip)
#if QT_CONFIG(shortcut)
        self.actionOuvrir.setShortcut(QCoreApplication.translate("MainWindow", u"Ctrl+O", None))
#endif // QT_CONFIG(shortcut)
        self.actionQuitter.setText(QCoreApplication.translate("MainWindow", u"Quitter", None))
#if QT_CONFIG(shortcut)
        self.actionQuitter.setShortcut(QCoreApplication.translate("MainWindow", u"Ctrl+Q", None))
#endif // QT_CONFIG(shortcut)
        self.actionShowTasksWoCont.setText(QCoreApplication.translate("MainWindow", u"N'afficher que les t\u00e2ches sans contexte", None))
        self.actionShowTasksWoPro.setText(QCoreApplication.translate("MainWindow", u"N'afficher que les t\u00e2ches sans projet", None))
        self.actionPref.setText(QCoreApplication.translate("MainWindow", u"Pr\u00e9f\u00e9rences", None))
        self.actionR_initialiser.setText(QCoreApplication.translate("MainWindow", u"R\u00e9initialiser", None))
#if QT_CONFIG(shortcut)
        self.actionR_initialiser.setShortcut(QCoreApplication.translate("MainWindow", u"Ctrl+R", None))
#endif // QT_CONFIG(shortcut)
        ___qtreewidgetitem = self.tree_tasks.headerItem()
        ___qtreewidgetitem.setText(4, QCoreApplication.translate("MainWindow", u"D\u00e9lai", None));
        ___qtreewidgetitem.setText(3, QCoreApplication.translate("MainWindow", u"\u00c9ch\u00e9ance", None));
        ___qtreewidgetitem.setText(2, QCoreApplication.translate("MainWindow", u"Description", None));
        ___qtreewidgetitem.setText(1, QCoreApplication.translate("MainWindow", u"Cr\u00e9ation", None));
        ___qtreewidgetitem.setText(0, QCoreApplication.translate("MainWindow", u"Priorit\u00e9", None));
        self.le_new_task.setPlaceholderText(QCoreApplication.translate("MainWindow", u"Nouvelle t\u00e2che", None))
        self.btn_add_task.setText(QCoreApplication.translate("MainWindow", u"Ajouter", None))
        self.menuFichiers.setTitle(QCoreApplication.translate("MainWindow", u"Fichier", None))
        self.dockWidget.setWindowTitle(QCoreApplication.translate("MainWindow", u"Vue", None))
        self.le_filter.setPlaceholderText(QCoreApplication.translate("MainWindow", u"Filtre libre", None))
        self.cb_filter_priority.setPlaceholderText(QCoreApplication.translate("MainWindow", u"Priorit\u00e9", None))
        self.cbox_priority.setText(QCoreApplication.translate("MainWindow", u"Priorit\u00e9s sup\u00e9rieures", None))
        self.cb_filter_context.setPlaceholderText(QCoreApplication.translate("MainWindow", u"@contexte", None))
        self.cb_filter_project.setPlaceholderText(QCoreApplication.translate("MainWindow", u"+projet", None))
        self.btn_clear_filter.setText(QCoreApplication.translate("MainWindow", u"R\u00e9initialiser", None))
    # retranslateUi

