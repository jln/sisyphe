from task import Task

class Tasks(list):
    def __init__(self, todofile, archivesfile):
        self.todofile = todofile
        self.archivesfile = archivesfile

    def filter_by_body(self, search):
        tasks = [task for task in self if search in task.body.lower()]
        return set(tasks)

    def filter_by_context(self, context):
        tasks = [task for task in self if task.context == context]
        return set(tasks)

    def filter_by_priority(self, priority, mode="sup"):
        if mode == "sup":
            tasks = [task for task in self if task.priority <= priority]
        elif mode == "only":
            tasks = [task for task in self if task.priority == priority]
        return set(tasks)

    def filter_by_project(self, project):
        tasks = [task for task in self if task.project == project]
        return set(tasks)

    def get_all_contexts(self, task_status="not_done"):
        if task_status == "not_done":
            contexts = [task.context for task in self
                        if task.context and not task.done]
        elif task_status == "done":
            contexts = [task.context for task in self
                        if task.context and task.done]
        elif task_status == "all":
            contexts = [task.context for task in self
                        if task.context]
        contexts.sort()
        return set(contexts)

    def get_all_projects(self, task_status="not_done"):
        if task_status == "not_done":
            projects = [task.project for task in self
                        if task.project and not task.done]
        elif task_status == "done":
            projects = [task.project for task in self
                        if task.project and task.done]
        elif task_status == "all":
            projects = [task.project for task in self
                        if task.project]
        projects.sort()
        return set(projects)

    def read_todofile(self):
        with open(self.todofile, "r") as file:
            lines = file.readlines()

        for line in lines:
            self.append(Task(line))

    def read_archivesfile(self):
        with open(self.archivesfile, "r") as file:
            lines = file.read().splitlines()

        for line in lines:
            self.append(Task(line))

    def write_todofile(self):
        with open(self.todofile, "w") as file:
            for task in self:
                if not task.done:
                    file.write(f"{task.task()}\n")

    def write_archivesfile(self):
        with open(self.archivesfile, "w") as file:
            for task in self:
                if task.done:
                    file.write(f"{task.task()}\n")

    def order_by_pri(self):
        self.sort(key=lambda x: x.priority)

    def get_tasks(self):
        return [task for task in self if task.done == False]

    #def delete_task(self, uuid):
    #    for index, task in enumerate(self):
    #        if task.uuid == uuid:
    #            del self[index]
    #            break



#coll_tasks = Tasks("todo.txt")
#coll_tasks.read_file()
#coll_tasks.imprimer_contenu()
